# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called 
by Pacman agents (in searchAgents.py).
"""

"""
Search algorithms implemented by Troy Zuroske
6 October 2016
CIS 571
"""


__author__ = 'zuroske'

import util

class SearchProblem:
  """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).
  
  You do not need to change anything in this class, ever.
  """
  
  def getStartState(self):
     """
     Returns the start state for the search problem 
     """
     util.raiseNotDefined()
    
  def isGoalState(self, state):
     """
       state: Search state
    
     Returns True if and only if the state is a valid goal state
     """
     util.raiseNotDefined()

  def getSuccessors(self, state):
     """
       state: Search state
     
     For a given state, this should return a list of triples, 
     (successor, action, stepCost), where 'successor' is a 
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental 
     cost of expanding to that successor
     """
     util.raiseNotDefined()

  def getCostOfActions(self, actions):
     """
      actions: A list of actions to take
 
     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
     util.raiseNotDefined()
           

def tinyMazeSearch(problem):
  """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
  from game import Directions
  s = Directions.SOUTH
  w = Directions.WEST
  return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first [p 85].

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm [Fig. 3.7].

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    '''My notes:'''
    '''problem.getStartState() = Start state is (5, 5) for mini maze'''
    '''isGoalState(problem.getStartState()) simply returns true or false'''
    '''getSuccessors returns [((5, 4), 'South', 1), ((4, 5), 'West', 1)] in
    order of: successor, action (West, South), stepCost'''

    # Create stack data structure
    stkShortestPath = util.Stack()
    # List to keep track of visited paths in graph or maze
    allVisitedNodes = []

    # Push the start state, an empty list for directions (no point of using
    # cost since all are the same)
    stkShortestPath.push((problem.getStartState(), []))

    currentNode, currentDirections = stkShortestPath.pop()

    # add start node to visited list
    allVisitedNodes.append(currentNode)

    # while we are not in the goal state continue looping
    while not problem.isGoalState(currentNode):

        # Loop through the successors of the returned node of the graph
        # stepCost not use except for looping
        for successor, direction, stepCost in problem.getSuccessors(
                currentNode):
            if successor not in allVisitedNodes:
                # Add to visited node to list
                allVisitedNodes.append(successor)
                # push new successors, directions onto stack
                stkShortestPath.push((successor, currentDirections +
                                      [direction]))

        # Pop the next state off stack with directions
        currentNode, currentDirections = stkShortestPath.pop()

    # return the DFS path found
    return currentDirections

def breadthFirstSearch(problem):
    "Search the shallowest nodes in the search tree first. [p 81]"
    "*** YOUR CODE HERE ***"

    # Create queue data structure
    queueBFShortestPath = util.Queue()
    # List to keep track of visited paths in graph or maze
    allVisitedNodes = []

    # Push the start state, an empty list for directions (no point of using
    # cost since all are the same)
    queueBFShortestPath.push((problem.getStartState(), []))

    currentNode, currentDirections = queueBFShortestPath.pop()

    # Add start node to visited list
    allVisitedNodes.append(currentNode)

    # while we are not in the goal state continue looping
    while not problem.isGoalState(currentNode):

        # Loop through the successors of the returned node of the graph
        # stepCost not use except for looping
        for successor, direction, stepCost in problem.getSuccessors(
                currentNode):

            if successor not in allVisitedNodes:
                # Add to visited node list
                allVisitedNodes.append(successor)
                # push new successors, directions, and cost onto stack
                queueBFShortestPath.push((successor, currentDirections +
                                      [direction]))

        # Pop the next state off queue with directions
        currentNode, currentDirections = queueBFShortestPath.pop()

    # return the BFS path found
    return currentDirections

def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"

    # Create priority queue
    pqueueUCS = util.PriorityQueue()
    # Create list for all visited nodes
    allVisitedNodes = [(None, None)]

    # Push the start state, an empty list for actions, and a zero cost to start
    pqueueUCS.push((problem.getStartState(), []), 0)

    currentNode, currentDirections = pqueueUCS.pop()

    # Create tuple list with a current node and cost of 0 to start
    allVisitedNodes.append((currentNode, 0))

    # while we are not in the goal state continue looping
    while not problem.isGoalState(currentNode):

        # Loop through the successors of the returned node of the graph
        # stepCost not use except for looping
        for successor, directions, stepCost in problem.getSuccessors(
                currentNode):
            oldOrExpensivePath = False
            # Get cost of current directions
            localCost = problem.getCostOfActions(currentDirections+[directions])
            for st, cost in allVisitedNodes:
                if (localCost >= cost) and (successor == st):
                    oldOrExpensivePath = True

            # If not a visited node or cheaper:
            if not oldOrExpensivePath:
                allVisitedNodes.append((successor, localCost))
                pqueueUCS.push((successor, currentDirections + [directions]),
                               localCost)

        # Pop the next state off queue with directions
        currentNode, currentDirections = pqueueUCS.pop()

    #return UCS path
    return currentDirections

def nullHeuristic(state, problem=None):
  """
  A heuristic function estimates the cost from the current state to the nearest
  goal in the provided SearchProblem.  This heuristic is trivial.
  """
  return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"

    # Create priority queue
    pqueueAStar = util.PriorityQueue()
    # Create list for all visited nodes
    allVisitedNodes = [(None, None)]

    # Push the start state, an empty list for actions, and a zero cost to start
    pqueueAStar.push((problem.getStartState(), []), 0)

    currentNode, currentDirections = pqueueAStar.pop()

    # Create tuple list with a current node and cost of 0 to start
    allVisitedNodes.append((currentNode, 0))

    # while we are not in the goal state continue looping
    while not problem.isGoalState(currentNode):

        # Loop through the successors of the returned node of the graph
        # stepCost not use except for looping
        for successor, directions, stepCost in problem.getSuccessors(
                currentNode):
            oldOrExpensivePath = False
            localCost = problem.getCostOfActions(
                currentDirections + [directions])
            # check each state and cost and see if we have been there or if it
            # is more expensive
            for st, cost in allVisitedNodes:
                if (localCost >= cost) and (successor == st):
                    oldOrExpensivePath = True

            # If not a visited node or cheaper
            if not oldOrExpensivePath:
                allVisitedNodes.append((successor, localCost))
                combineDirections = currentDirections + [directions]
                # get heuristic with cost to push
                test = heuristic(successor, problem) + \
                    problem.getCostOfActions(combineDirections)
                pqueueAStar.push((successor, combineDirections), test)

        # Pop the next state off queue with directions
        currentNode, currentDirections = pqueueAStar.pop()

    #return a star path
    return currentDirections
    
  
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch