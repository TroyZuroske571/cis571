# buyLotsOfFruit.py
# -----------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
To run this script, type

  python buyLotsOfFruit.py.py

Once you have correctly implemented the buyLotsOfFruit.py function,
the script should produce the output:

Cost of [('apples', 2.0), ('pears', 3.0), ('limes', 4.0)] is 12.25
"""

# Implemented by Troy Zuroske
# September 27, 2016

fruitPrices = {'apples': 2.00, 'oranges': 1.50, 'pears': 1.75,
               'limes': 0.75, 'strawberries': 1.00}


def buyLotsOfFruit(orderList):
    """
        orderList: List of (fruit, numPounds) tuples

    Returns cost of order
    """
    total_cost = 0.0

    for fruit in orderList:
        if fruit[0] in fruitPrices.keys():
            total_cost += fruit[1] * fruitPrices[fruit[0]]
        else:
            print 'Error', fruit[0], 'does not exist.'
            return 'Error'

    return total_cost


# Main Method
if __name__ == '__main__':
    "This code runs when you invoke the script from the command line"
    orderList = [('apples', 2.0), ('pears', 3.0), ('limes', 4.0)]
    if buyLotsOfFruit(orderList) == 'Error':
        print 'Total cost not calculated.'
    else:
        print 'Cost of', orderList, 'is', buyLotsOfFruit(orderList)
